FROM debian

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y apt-utils && \
    apt-get install -y autoconf build-essential cmake git libtool pkg-config

RUN git clone --recurse-submodules -b v1.37.1 https://github.com/grpc/grpc

# build grpc
RUN cd grpc && \
    mkdir -p cmake/build && \
    cd cmake/build && \
    cmake -DgRPC_INSTALL=ON \
        -DgRPC_BUILD_TESTS=OFF \
        ../.. && \
    make -j && \
    make install

# build abseil
RUN cd grpc/third_party/abseil-cpp && \
    mkdir build && cd build && \
    cmake .. && \
    cmake --build . --target install

# build example
RUN cd grpc/examples/cpp/helloworld && \
    mkdir -p cmake/build && \
    cd cmake/build && \
    cmake ../.. && \
    make -j
